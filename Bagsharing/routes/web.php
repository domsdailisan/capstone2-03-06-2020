<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('categories', 'CategoryController');
Route::resource('designers', 'DesignerController');
Route::resource('conditions', 'ConditionController');

Route::get('/', 'ProductController@index');
Route::post('/search/designer', 'ProductController@search');
Route::put('/products/{id}/restore', 'ProductController@restore');

Route::resource('products', 'ProductController', [
"except" => ["show", "index"]])->middleware("isAdmin");

Route::resource("products", "ProductController", [
"only" => ["show", "index"]]);

// Route::resource('products', 'ProductController');

Route::delete('cart/empty', 'CartController@emptyCart');
Route::get('/cart/confirm', "CartController@confirmOrder");

Route::resource('cart', 'CartController');

Route::patch('/orders/{order}/confirm', 'OrderController@confirmReturn');
Route::patch('/orders/{order}/return', 'OrderController@return');
Route::post('/search/transaction', 'OrderController@search');
Route::resource('orders', 'OrderController');




