<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id'); //product id

            $table->unsignedBigInteger('designer_id'); //id coming from designers table
            $table->foreign('designer_id')
                    ->references('id')
                    ->on('designers')
                    ->onDelete('restrict') //di basta basta madedelete if related s ibang table
                    ->onUpdate('cascade');

            $table->string('name'); //product name

            $table->string('color');

            $table->unsignedBigInteger('condition_id'); //id coming from conditions table
            $table->foreign('condition_id')
                    ->references('id')
                    ->on('conditions')
                    ->onDelete('restrict'); //di basta basta madedelete if related s ibang table

            $table->text('description');//product description
            
            $table->decimal('rent', 10, 2);// column name, number of digits, decimal places. will be computed 20% of price

            $table->decimal('price', 10, 2);// column name, number of digits, decimal places. buy out price

            $table->unsignedBigInteger('category_id'); //id coming from categories table
            $table->foreign('category_id')
                    ->references('id')
                    ->on('categories')
                    ->onDelete('restrict'); //di basta basta madedelete if related s ibang table

            $table->integer('stock');

            $table->string('img_path');
            
            $table->boolean('isActive')->default(true);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
