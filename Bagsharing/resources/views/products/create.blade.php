@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row mt-4">
		<div class="col-lg-8 bg-light rounded py-2">
			<form method="POST" action="/products" enctype="multipart/form-data">
				@csrf

				<div class="form-group">
					<label for="name">Bag Name: </label>
					<input type="text" name="name" class="form-control">
				</div>

				<div class="form-group">
					<label for="color">Color: </label>
					<input type="text" name="color" class="form-control">
				</div>

				<div class="form-group">
					<label for="description">Description: </label>
					<textarea name="description" class="form-control"></textarea>
				</div>

				<div class="form-group">
					<label for="stock">Stock: </label>
					<input type="number" name="stock" min=0 class="form-control">
				</div>

				<div class="form-group">
					<label for="price">Price: </label>
					<input type="number" name="price" step="0.01" min=0 class="form-control">
				</div>

				<div class="form-group">
					<label for="image">Upload Image: </label>
					<input type="file" name="image" id="image" class="form-control">
				</div>
				<div class="row  text-center">
					<div class="form-group col-lg-4">
						<p><strong>Designer</strong></p>
						<select name="designer">
							@foreach($designers as $designer)
								<option value="{{$designer->id}}">{{$designer->name}}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group col-lg-4">
						<p><strong>Condition</strong></p>
						<select name="condition">
							@foreach($conditions as $condition)
								<option value="{{$condition->id}}">{{$condition->name}}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group col-lg-4">
						<p><strong>Category</strong></p>
						<select name="category">
							@foreach($categories as $category)
								<option value="{{$category->id}}">{{$category->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-success">Add new product</button>
				</div>
			</form>
		</div>
		<div class="col-lg-4 text-center bg-dark text-white rounded pt-2">
			<form action="/designers" method="POST">
				@csrf
				<div class="form-group">
					<label for="name">Designer: </label>
					<input type="text" name="designer" class="form-control">
				</div>
				<button type="submit" class="btn btn-success">Add new designer</button>
			</form>
			<hr class="bg-white">
			<form action="/conditions" method="POST">
				@csrf
				<div class="form-group">
					<label for="name">Condition: </label>
					<input type="text" name="condition" class="form-control">
				</div>
				<button type="submit" class="btn btn-success">Add new condition</button>
			</form>
			<hr class="bg-white">
			<form action="/categories" method="POST">
				@csrf
				<div class="form-group">
					<label for="name">Category: </label>
					<input type="text" name="category" class="form-control">
				</div>
				<button type="submit" class="btn btn-success">Add new category</button>
			</form>
			<hr class="bg-white">
				<a href="/products" class="btn btn-info mt-5">Go To Catalog</a>
		</div>
	</div>
	</div>

</div>
@endsection

<!-- 
Designers list
-Balenciaga
-Chanel
-Dior
-Dolce & Gabbana
-Gucci
-Hermes
-Kate Spade
-Louis Vuitton
-Prada
-Saint Laurent
-Versace

Condition list
-Brand New
-Pristine > in its original condition; unspoiled.
-Pre-Loved
-Vintage

Category
-Backpacks
-Belt Bags
-Briefcases
-Cases
-Cosmetic Bags
-Crossbody Bags
-Duffle Bags
-Messenger Bags
-Pochettes
-Shoulder Bags


 -->