@extends('layouts.app')

@section('content')
	{{-- SHOWCASE 
	Chanel        Dior           Gucci 
	Louis Vuitton Celine         Prada  
	Valentino     Saint Laurent  Hermes
		--}}
	<div class="row bg-img">
		@for($i=1; $i<10; $i++)
			<div class="col-lg-4 col-md-6 designers my-2 text-center">
				<img src="{{asset('images/designer-' . $i .'.jpg')}}" style="height: 100px;">
			</div>
		@endfor
	</div>

	@if(Session::has('message'))
		<h3 class="text-center text-flash py-2">{{Session::get('message')}}</h3>
	@endif
	
	<div class="row m-4 bag-filter">
		<div class="col-2 my-2">
			{{-- Filtering --}}
			{{-- Add a button for ALL --}}
			{{-- <a href="/products" class="btn btn-primary">CATEGORIES</a> --}}
			<ul class="list-group text-center">
				<li class="list-group-item active mb-1"><a href="/products" class="text-white"><strong>ALL</strong></a></li>
				{{-- CATEGORIES FILTER --}}
				<li class="list-group-item active">CATEGORIES</li>
				{{-- Add button for each category --}}
				@foreach(App\Category::all() as $indiv_category)
					{{-- <a href="/categories/{{$indiv_category->id}}" class="btn btn-primary">{{$indiv_category->name}}</a> --}}
					<li class="list-group-item"><a href="/categories/{{$indiv_category->id}}">{{$indiv_category->name}}</a></li>
				@endforeach
				{{-- DESIGNERS FILTER --}}
				<li class="list-group-item active">DESIGNERS</li>
				@foreach(App\Designer::all() as $indiv_designer)
					
					<li class="list-group-item"><a href="/designers/{{$indiv_designer->id}}">{{$indiv_designer->name}}</a></li>
				@endforeach
				{{-- CONDITIONS FILTER --}}
				<li class="list-group-item active">CONDITIONS</li>
				@foreach(App\Condition::all() as $indiv_condition)
					<li class="list-group-item"><a href="/conditions/{{$indiv_condition->id}}">{{$indiv_condition->name}}</a></li>
				@endforeach
			</ul>
		</div>
		<div class="col-10">
			<div class="row">
				@foreach($products as $indiv_product)
					<div class="col-lg-4 col-md-6 my-2">
						<div class="card">
							<img src="{{asset($indiv_product->img_path)}}" class="card-img-top" style="height:200px;" alt="{{$indiv_product->name.' image'}}">
							<div class="card-body">
								<h5 class="card-title">Designer: <strong>{{$indiv_product->designer->name}}</strong></h5>
								<p class="card-text mb-0">Bag Name: <strong>{{$indiv_product->name}}</strong></p>
								<p class="card-text mb-0">Color: <strong>{{$indiv_product->color}}</strong></p>
								<p class="card-text mb-0">Description: <em>{{$indiv_product->description}}</em></p>
								<p class="card-text mb-0">Rent/month: <strong>{{$indiv_product->rent}} USD</strong></p>
								<p class="card-text mb-0">Price: <strong>{{$indiv_product->price}} USD</strong></p>
								<p class="card-text mb-0">Stock: {{-- {{$indiv_product->stock}} --}}
									<strong><em>
										{{-- @if($indiv_product->isActive) --}}
										@if($indiv_product->deleted_at == null && $indiv_product->stock > 0 && $indiv_product->isActive)
											Available
										@else
											Not Available
										@endif
									</em></strong>
								</p>
								<p class="card-text mb-0">Category: <strong>{{$indiv_product->category->name}}</strong></p>
								

								{{-- @if(!Auth::check() || !Auth::user()->isAdmin) --}}
								@if($indiv_product->deleted_at == null && $indiv_product->stock > 0 && $indiv_product->isActive)
									{{-- RENT BUTTON --}}
									<label class="btn btn-secondary d-block">
										<form method="POST" action="/cart">
											@csrf
											<input type="hidden" name="item_id" value="{{$indiv_product->id}}">
											<input type="number" name="quantity" class="form-control mb-1" min="1" value="1">
											<button type="submit" name="rent" class="btn btn-secondary form-control border-white" value="Rent"><strong>R e n t</strong></button>
											{{-- <button type="submit" name="buy" class="btn btn-success d-inline border-white mx-4" value="Buy"><strong>Buy</strong></button> --}}
										</form>
									</label>
								@endif
								@if(Auth::check())
									<a href="/products/{{$indiv_product->id}}" class=" btn btn-secondary d-block text-white mt-2">View Details</a>
								@endif
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>

{{-- FOOTER --}}
	<footer class="bg-dark py-5">
		<div class="text-center text-muted">
			<p><i class="fas fa-phone"></i> +63-947-xxx-xxxx</p>
			<p><i class="fas fa-mail-bulk"></i> rentabag@mail.com</p>
		</div>
		<div class="container">
			<div class="text-center text-muted">Copyright &copy; <strong>Rent-A-Bag</strong></div>
		</div>
	</footer>

@endsection