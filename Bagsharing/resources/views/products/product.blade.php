@extends('layouts.app')

@section('content')

	<div class="row">
		<div class="col-lg-6 offset-lg-3">
			<div class="card">
				<img src="{{asset($product->img_path)}}" class="card-img-top" alt="{{$product->name.' image'}}">
				<div class="card-body">
					<h5 class="card-title">Designer: <strong>{{$product->designer->name}}</strong></h5>
					<p class="card-text">Bag Name: <strong>{{$product->name}}</strong></p>
					<p class="card-text">Description: <em>{{$product->description}}</em></p>
					<p class="card-text">Rent/month: <strong>{{$product->rent}} USD</strong></p>
					<p class="card-text">Price: <strong>{{$product->price}} USD</strong></p>
					@if(Auth::user()->isAdmin)
						<p class="card-text">Stock: <strong>{{$product->stock}} pc(s)</strong></p>
					@elseif($product->deleted_at == null && $product->stock > 0 && $product->isActive)
						<p class="card-text">Stock: <strong>Available</strong></p>
					@else
						<p class="card-text">Stock: <strong>Not Available</strong></p>
					@endif
					<p class="card-text">Category: <strong>{{$product->category->name}}</strong></p>

					<div class="btn-group btn-group-toggle" data-toggle="buttons">
						<label class="btn btn-secondary border-white">
							<!-- <input type="radio" name="options" id="option1"> Rent -->
							<a href="/products" class="btn text-white">Back to Catalog</a>
						</label>
						@if(!Auth::check() || Auth::user()->isAdmin)
							<label class="btn btn-primary border-white">
								<!-- <a> tag lang sa edit since mag rereturn lang naman yan ng edit view -->
								<a href="/products/{{$product->id}}/edit" class="btn text-white">Edit</a>
							</label>
							@if($product->deleted_at == null)
								<label class="btn btn-danger border-white">
									<form action="/products/{{$product->id}}" method="POST">
										@csrf
										@method("DELETE")
										<button type="submit" class="btn btn-danger">Delete</button>
									</form>
								</label>
							@else
								<label class="btn btn-success border-white">
									<form action="/products/{{$product->id}}/restore" method="POST">
										@csrf
										@method("PUT")
										<button type="submit" class="btn btn-success">Restore</button>
									</form>
								</label>
							@endif
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>


@endsection