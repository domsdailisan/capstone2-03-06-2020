@extends('layouts.app')

@section('content')
<div class="mycart">	
	@if(Session::has('message'))
		<h4 class="text-center text-flash py-2">{{Session::get('message')}}</h4>
	@endif

	<div class="col-lg-8 offset-lg-2 bg-white py-2">
		<h2 class="text-center">My Cart</h2>
		<table class="table table-striped text-center">
			<thead>
				<th>Designer</th>
				<th>Bag Name</th>
				<th>Description</th>
				<th>Price</th>
				<th>Quantity</th>
				<th>Subtotal</th>
				<th></th>
			</thead>
			
			<tbody>
				@if(!empty($items_in_cart))
					@foreach($items_in_cart as $indiv_product)
						<tr>
							<td>{{$indiv_product->designer->name}}</td>
							{{-- <td>
								{{$indiv_product->name}}
								<img src="{{asset($indiv_product->img_path)}}" alt="current_img" style="height:50px; width:50px">
							</td> --}}
							<td>
								<div class="row">
									<div class="col-lg-8">
										{{$indiv_product->name}}
									</div>
									
									<div class="col-lg-4">
										<img src="{{asset($indiv_product->img_path)}}" alt="current_img" style="height:50px; width:50px">
									</div>
								</div>
							</td>
							<td>{{$indiv_product->description}}</td>
							<td>{{$indiv_product->price}} USD</td>
							<td>
								{{-- <input type="number" name="quantity" value="{{$indiv_product->quantity}}"> --}}
								<form method="POST" action="/cart/{{$indiv_product->id}}">
									@csrf
									@method("PATCH")
									<div class="row">
										<div class="col-lg-8">
											<input type="number" name="newqty" value={{$indiv_product->quantity}} min=1 class="form-control">
										</div>
										<div class="col-lg-4">
											<button type="submit" class="btn btn-info text-center"><small>update</small></button>
										</div>
									</div>
								</form>
							</td>
							<td>{{$indiv_product->subtotal}} USD</td>
							<td>
								<form action="/cart/{{$indiv_product->id}}" method="POST">
									@csrf
									@method("DELETE")
									{{-- POST and GET are the HTTP verbs that are supported by method, we override it to become DELETE --}}
									<button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
								</form>
							</td>
						</tr>

					@endforeach

						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th>TOTAL</th>
						<th>{{$total}}</th>
						<th></th>
				@else
					<tr>
						<td colspan="7">Cart is empty</td>
					</tr>
				@endif
			</tbody>
		</table>

		
		<form action="/cart/empty" method="POST">
			@csrf
			@method("DELETE")
			@if(!empty($items_in_cart))
				<button type="submit" class="btn btn-danger">Empty Cart</button>

			
				@if(!Auth::check())
					<a href="/login" class="btn btn-success">Checkout</a>
				@else
					<a href="/cart/confirm" class="btn btn-success">Checkout</a>
				@endif

				<a href="/products" class="btn btn-primary">Add more products</a>
			@else
				<a href="/products" class="btn btn-primary">Add products</a>
			@endif
		</form>
	</div>
</div>
@endsection