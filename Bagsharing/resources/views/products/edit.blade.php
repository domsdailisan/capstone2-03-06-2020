@extends('layouts.app')


@section('content')
	<div class="row mt-4">
		<div class="col-lg-8 offset-lg-2">
			<form method="POST" action="/products/{{$product->id}}" enctype="multipart/form-data">
				@csrf
				@method("PUT")
				<div class="form-group">
					<label for="name">Bag Name: </label>
					<input type="text" name="name" class="form-control" value="{{$product->name}}">
				</div>

				<div class="form-group">
					<label for="description">Description: </label>
					<textarea name="description" class="form-control">{{$product->description}}</textarea>
				</div>

				<div class="form-group">
					<label for="qty">Stock: </label>
					<input type="number" name="stock" min=0 class="form-control" value="{{$product->stock}}">
					{{-- <form method="POST" action="/products/{{$product->id}}"> --}}
						<div class="mt-2">
							<input type="number" name="addStock" class="round" placeholder="-------additional stock-------">
							<strong>Additional Stock Here</strong>
						</div>
					{{-- </form> --}}
				</div>

				<div class="form-group">
					<label for="price">Price: </label>
					<input type="number" name="price" step="0.01" min=0 class="form-control" value="{{$product->price}}">
				</div>

				<div class="form-group">
					<img src="{{asset($product->img_path)}}" alt="current_img" style="height:50px; width:50px">
					<label for="image">Upload Image: </label>
					<input type="file" name="image" id="image" class="form-control">
				</div>
				<div class="row  text-center">
					<div class="form-group col-lg-4">
						<p><strong>Designer</strong></p>
						<select name="designer">
							@foreach($designers as $designer)
								<option value="{{$designer->id}}"{{$product->designer_id == $designer->id ? "selected" : ""}}>{{$designer->name}}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group col-lg-4">
						<p><strong>Condition</strong></p>
						<select name="condition">
							@foreach($conditions as $condition)
								<option value="{{$condition->id}}"{{$product->condition_id == $condition->id ? "selected" : ""}}>{{$condition->name}}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group col-lg-4">
						<p><strong>Category</strong></p>
						<select name="category">
							@foreach($categories as $category)
								<option value="{{$category->id}}"{{$product->category_id == $category->id ? "selected" : ""}}>{{$category->name}}</option>
							@endforeach
						</select>
					</div>
				</div>

				<button type="submit" class="btn btn-success"><strong>Done Editing</strong></button>
			</form>
		</div>
	</div>									
@endsection