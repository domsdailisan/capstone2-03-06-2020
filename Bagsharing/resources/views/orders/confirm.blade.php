@extends('layouts.app')
@section('content')
<div class="confirmorder">
	<div class="col-lg-6 offset-lg-3 bg-white py-2">
		<h2 class="text-center">Confirm Order</h2>
		<table class="table table-striped">
			<thead>
				<th>Designer Name</th>
				<th>Bag Name</th>
				<th>Price</th>
				<th>Qty</th>
				<th>Subtotal</th>
			</thead>

			<tbody>
				@foreach($items_in_cart as $indiv_product)
				<tr>
					<td>{{$indiv_product->designer->name}}</td>
					<td>{{$indiv_product->name}}</td>
					<td>{{$indiv_product->price}} USD</td>
					<td>{{$indiv_product->quantity}}</td>
					<td>{{$indiv_product->subtotal}} USD</td>
				</tr>
				@endforeach

				<th></th>
				<th></th>
				<th></th>
				<th>TOTAL</th>
				<th>{{$total}}</th>

			</tbody>

		</table>

		<form action="/orders" method="POST">
			@csrf
			<button type="submit" class="btn btn-success">Confirm Order</button>
		</form>
	</div>
</div>
@endsection