@extends('layouts.app')
@section('content')
	<div class="review text-center">
		<div class="review-text">
			<h2 class="text-center">Return order <span style="color:green;">{{$refNo}}</span> is now subjected for Return Confirmation.</h2>

			<h3>Thank you very much!</h3>
			<a href="/orders" class="btn bg-white text-center">Back to Order List</a>
		</div>
	</div>
@endsection