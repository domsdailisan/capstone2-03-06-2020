@extends('layouts.app')
@section('content')
<div class="result text-center">
	<div class="result-text">
		<h2 class="text-center">Your order <span style="color:green;">{{$refNo}}</span> is now pending for approval!</h2>
		<a href="/orders" class="btn bg-white text-center">Back to Order List</a>
	</div>
</div>
@endsection