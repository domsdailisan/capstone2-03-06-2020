@extends("layouts.app")
@section("content")
<div class="orderlist">
		<div class="my-4 d-flex justify-content-center">
			<form class="form-inline my-2 my-lg-0" action="/search/transaction" method="POST">
				@csrf
				<input class="form-control mr-sm-2" type="text" placeholder="Input Ref No." aria-label="Search" name="search">
				<button class="btn btn-info my-2 mr-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
			</form>
		</div>

			<div class="row text-center">
				<div class="col-lg-6 offset-lg-3">
					<h4 class="text-white">Transaction History</h4>
				</div>
				
				<div class="col-lg-8 offset-lg-2 bg-white py-2">
					<table class="table table-striped table-bordered">
						<thead>
							<th>Reference No.</th>
							<th>User</th>
							<th>Total</th>
							<th>Status</th>
							<th>Details</th>
							
							@if(Auth::user()->isAdmin)
								<th>Current Stock</th>
								<th>Action</th>
							@else
								<th>Action</th>
							@endif
						</thead>
						<tbody>
							@foreach($orders as $order)
								<tr>
									<td>{{$order->refNo}}</td>
									<td>{{$order->user->name}}</td>
									<td>{{$order->total}} USD</td>
									<td>{{$order->status->name}}</td>
									<td>
										@foreach($order->products as $product)
										{{-- $order->products used the pivot table products_orders to look at the details of ALL the products linked to the order --}}
											<p>Name: {{$product->name}}, x {{$product->pivot->quantity}}</p>
											{{-- pivot refers to the columns assosiated to the product in the pivot table --}}
										@endforeach
									</td>
									
									@if(Auth::user()->isAdmin)
										<td>
											@foreach($order->products as $product)
												<p>Stock: {{$product->stock}}</p>
											@endforeach
										</td>
									@endif
										<td>
											@if($order->status_id == 1 && Auth::user()->isAdmin)
												<form action="/orders/{{$order->id}}" method="POST">
													@csrf
													@method("PATCH")
													<button type="submit" class="btn btn-success">Complete Order</button>
												</form>

												<form action="/orders/{{$order->id}}" method="POST">
													@csrf
													@method("DELETE")
													<button type="submit" class="btn btn-danger mt-2">Cancel Order</button>
												</form>
											@endif

											@if($order->status_id == 2 && !Auth::user()->isAdmin)
												<form action="/orders/{{$order->id}}/return" method="POST">
													@csrf
													@method("PATCH")
													<button type="submit" class="btn btn-success mt-2">Return</button>
												</form>
											@endif


											@if($order->status_id == 4 && Auth::user()->isAdmin)
												<form action="/orders/{{$order->id}}/confirm" method="POST">
													@csrf
													@method("PATCH")
													<button type="submit" class="btn btn-success mt-2">Confirm Return</button>
												</form>
											@endif

											
										</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
</div>
@endsection