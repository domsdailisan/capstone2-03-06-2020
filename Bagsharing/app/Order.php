<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function products(){
    	return $this->belongsToMany("\App\Product", 'products_orders')
    				->withPivot('quantity', 'subtotal')
    				->withTimeStamps();
    }

    public function status(){
    	return $this->belongsTo("\App\Status");
    }

    public function user(){
    	return $this->belongsTo("\App\User");
    }
}
