<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use softDeletes;

    public function category(){
    	return $this->belongsTo("\App\Category");
    }

    public function designer(){
    	return $this->belongsTo("\App\Designer");
    }

    public function condition(){
    	return $this->belongsTo("\App\Condition");
    }

    public function orders(){
    	return $this->belongsToMany("\App\Order", 'products_orders')
    				->withPivot('quantity', 'subtotal')
    				->withTimeStamps();
    }
}
