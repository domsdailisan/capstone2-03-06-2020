<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use \App\Product;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // session()->forget('cart');
        // dd(session()->get('cart'));
        $items_in_cart = [];
        $total = 0;

        if(Session::exists('cart') || Session::get('cart') != null){
            foreach(Session::get('cart') as $item_id => $quantity){
                //because session cart has keys(item_id) and values(quantity)
                //find the item
                // dd($quantity);
                $product = Product::find($item_id);
                //get the details needed
                // dd($product);
                $product->quantity = $quantity;
                $product->subtotal = $product->price * $quantity;
                //Note: these properties (quantity and subtotal ARE NOT part of the Product stored in the database, they are only for $product)
                //push to array containing the details
                //Syntax: array_push(target array, data to be pushed)
                array_push($items_in_cart, $product);
                //send the array to the view
                $total += $product->subtotal;
            }
        }
        return view('products.cart', compact('items_in_cart', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request); // returns item_id, quantity, rent
        $product = Product::find($request->item_id);
        //dd($product); returns all the details on that id
        $cart = [];//create an empty cart
        if ($product->stock >= $request->quantity){
            if (Session::exists('cart')){
                $cart = Session::get('cart');
            }
            $cart[$request->item_id] = $request->quantity;
            Session::put('cart', $cart);
        }else {
            Session::flash('message', "Sorry but you've inputed a quantity more than what we can offer, Please input lower quantity. Thank you!");
            // "Your request $request->quantity is higher than our current stock."
        }
        return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($id);
        $product = Product::find($id);
        // dd($product);
        // dd($request);
        if ($product->stock >= $request->newqty){
            $cart = Session::get("cart");
            $cart[$id] = $request->newqty;
            Session::put("cart", $cart);
        }else{
            Session::flash('message', "Sorry but you've inputed a quantity more than what we can offer, Please input lower quantity. Thank you!");
        }
        
        return redirect("/cart");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Session::forget("cart.$id"); 
        return redirect('/cart');
    }

    public function emptyCart(){
        // dd(Session::exists('cart'));
        if(Session::exists('cart')){
            //Remove all cart entries
            Session::forget('cart');
        }
        return redirect('/cart');
    }

    public function confirmOrder(){
        $items_in_cart = [];
        $total = 0;

        if(Session::exists('cart')|| Session::get('cart') != null){
            foreach(Session::get('cart') as $item_id => $quantity){
                $product = Product::find($item_id);
                $product->quantity = $quantity;
                $product->subtotal = $product->price * $quantity;
                array_push($items_in_cart, $product);
                $total += $product->subtotal;
            }
        }
        return view('orders.confirm', compact('items_in_cart', 'total'));
    }
}
