<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Auth;
use App\Product; //use the product model
use App\Status; //use the status model
use Session; //to get the cart

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){
            $orders = [];
            if (Auth::user()->isAdmin) {
                $orders = Order::all();
            }else{
                $orders = Order::where("user_id", Auth::user()->id)->get();
            }
            return view('orders.orderlist', compact('orders'));
        }else{
            return redirect("/login");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //Create a new order with total = 0;
        $refNo = "Rent-A-Bag-".time();
        $new_order = new Order;
        $new_order->refNo = $refNo;
        $new_order->user_id = Auth::user()->id;
        $new_order->status_id = 1; //always pending (default)
        $new_order->total = 0;
        // dd($new_order);
        $new_order->save();
        $total = 0;
        //Attach the products to the new order -> write to the products_orders table
        foreach(Session::get("cart") as $item_id => $quantity){
            $product = Product::find($item_id); //to get the price property
            $subtotal = $product->price*$quantity;
            //checklist to attached: order_id($order), item_id($item_id), quantity($quantity), subtotal($subtotal)
            //to populate the products_orders table
            $new_order->products()->attach($item_id, ["quantity" => $quantity, "subtotal" => $subtotal]);
            //"For this order, get the pivot table and create a new row with its order_id, item_id and the quantity and subtotal field"
            //The 2nd parameter of attach (associative array) contains all the pivot columns
            //update the total
            $total += $subtotal;
        }
        //Update the new order's total
        $new_order->total = $total;
        $new_order->save();
        Session::forget("cart");
        // return "Order Successfully Created!";
        return view("orders.result", compact("refNo"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $products = $order->products;
     
        foreach($products as $product){
            //dd($product->pivot->quantity);
            $prodUp = Product::find($product->id);
            $stock = $product->stock;
            $quantity = $product->pivot->quantity;
            $prodUp->stock -= $quantity;
            $prodUp->save();
        };

        $order->status_id = 2;
        $order->save();
        return redirect("/orders");
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->status_id = 3;
        $order->save();
        return redirect("/orders");
    }

    public function return(Request $request, Order $order)
    {
        // // dd($order);
        // $products = $order->products;
        // foreach($products as $product){
        //     $returnprod = Product::find($product->id);
        //     // dd($returnprod); //7
        //     $stock = $product->stock;
        //     $quantity = $product->pivot->quantity;
        //     $returnprod->stock += $quantity;
        //     // dd($returnprod->stock); //8

            // dd($order);
            $order->status_id = 4;
            $order->save();
            $refNo = $order->refNo;
            // $returnprod->save();
        // }
        return view("orders.review", compact("refNo"));
    }

    public function confirmReturn(Request $request, Order $order)
    {
        // dd($order);
        $products = $order->products;
        foreach($products as $product){
            $returnprod = Product::find($product->id);
            // dd($returnprod);
            $stock = $product->stock;
            $quantity = $product->pivot->quantity;
            $returnprod->stock += $quantity;
            // dd($returnprod->stock);


            $order->status_id = 5;
            $order->save();
            $returnprod->save();
        }
        return redirect("/orders");
    }

    public function search(Request $request)
    {
        $query = $request->search;
        // dd($query);
        if ($query != null) {
            if(Auth::user()->isAdmin){
                $orders = Order::where('refNo', 'LIKE', '%'.$query.'%')->get();
            }else{
              $orders = Order::where('refNo', 'LIKE', '%'.$query.'%')->where('user_id', Auth::user()->id)->get();  
            }
            
        }

        return view('orders.orderlist', compact('orders'));
    }


}
