<?php

namespace App\Http\Controllers;

use App\Product;
use App\Designer;
use App\Condition;
use App\Category;
use Illuminate\Http\Request;
use Session;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $products = Product::all();
        $products = Product::withTrashed()->get();

        return view('products.catalog', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $designers = Designer::all();
        $conditions = Condition::all();
        $categories = Category::all();
        return view('products.create', compact('designers', 'conditions', 'categories'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request); // test to check the passed values
        $product = new Product;
        $product->designer_id = $request->designer; //foreign key for designer
        $product->condition_id = $request->condition;//foreign key for condition
        $product->category_id = $request->category;//foreign key for category
        $product->name = $request->name;
        $product->color = $request->color;
        $product->description = $request->description;
        $product->stock = $request->stock;
        $product->rent = $request->price * 0.2; //20% of original price inputed on the product price
        $product->price = $request->price;
        
        $image = $request->file('image');
        $image_name = time().".".$image->getClientOriginalExtension();
        $destination = "images/"; //this will automatically create a images folder under public
        $image->move($destination, $image_name);
        $product->img_path = $destination.$image_name;
        $product->save();

        //dd($product); //check if nakukuha natin lahat ng inputs
        // return redirect('products');
        return redirect('products/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    // public function show(Product $product)
    public function show($id)
    {
        //dd($product); //to check what details are being passed
        $product = Product::withTrashed()->where("id", $id)->first();

        return view('products.product', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //dd($product); // to check if routed correctly
        $designers = Designer::all();
        $conditions = Condition::all();
        $categories = Category::all();

        return view('products.edit', compact('designers', 'conditions', 'categories', 'product'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //dd($request); //this are the details of your inputs on edit from
        // dd($product); this are the details of the original product

        $product->designer_id = $request->designer;
        $product->name = $request->name;
        $product->condition_id = $request->condition;
        $product->description = $request->description;
        $product->rent = $request->price * 0.2;
        $product->price = $request->price;
        $product->category_id = $request->category;
        if ($request->addStock != null){
            $product->stock = $request->stock + $request->addStock;
        }else{
            $product->stock = $request->stock;
        }

        $image = $request->file('image');

        if ($image != ""){
            $image_name = time().".".$image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $image_name);
            $product->img_path = $destination.$image_name;
        }

        $product->save();

        return redirect("/products/$product->id"); //if di ka gagamitin ng double quote "" ito ang syntax mo return redirect('/products'.$product->id)

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect("/products");
    }

    public function upStock()
    {
        // $products = Product::all();
        return view("/products/updateStock");
    }

    public function search(Request $request)
    {
        $query = $request->search;

        if ($query != null) {
            $products = Product::where('name', 'LIKE', '%'.$query.'%')->get();
        }

        return view('products.catalog', compact('products'));
    }

    public function restore($id)
    {
        $product = Product::withTrashed()->where("id", $id)->first();
        $product->restore();

        return view('products.product', compact('product'));
    }
}
